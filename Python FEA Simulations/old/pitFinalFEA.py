import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as patches
import matplotlib.colors as colors

# PHYSICAL PARAMETERS
u0 = 4e-7*np.pi
m = np.array([0,1]) # every dipole will have the same magnetization vector, Bohn Magneton is 9.274e-24 J/T for context
Idl = np.array((0,0,-10)) # current flow vector for biot-savart

def calc_biot_H(r):
    r_mag = np.sqrt(r.dot(r))
    r_hat = r/r_mag
    biot_H = 1/(4*np.pi*r_mag**2)*(np.cross(Idl,r_hat))
    #print(biot_H)
    return biot_H # numpy will automatically resolve the z componenet, so no need to delete it or append it to r_hat initially

def calc_dipo_H(r, internalCheck):
    r_mag = np.sqrt(r.dot(r)) # the magnitude of r (faster than np.linalg.norm(r))
    r_hat = r/r_mag # unit vector is the vector divided by its magnitude
    dipo_H = 1/(4*np.pi*r_mag**3)*(3*np.dot(r_hat,m)*r_hat - m) 
    if internalCheck == 4: # dirac delta is essentially all of the if statements that led us here...
        dipo_H = dipo_H - 1/(3)*m # ***artificially swapped sign of this term
    return dipo_H

if __name__ == '__main__':
# BUILD 2D SIMULATION
    x_size = 30
    y_size = 30

    elements = np.zeros((x_size,y_size,4))
    '''
    elements[0] is a tristate where 0=air, 1=dipole, and 2=wire
    elements[1] is the x component of the H field
    elements[2] is the y component of the H field
    elements[3] is the magnitude of that H vector
    '''

    # Now let's assign a region of permanent magnet
    mag_w = 6
    mag_h = 10
    mag_o = [int(x_size/2-mag_w/2), int(y_size/2-mag_h/2)]
    elements[mag_o[0]:mag_o[0]+mag_w, mag_o[1]:mag_o[1]+mag_h] = [1,0,0,0]

    # Now let's assign a region of current carrying wire
    wire_w = 2
    wire_h = 2
    wire_o = [6, int(x_size/2)]#[int(x_size/2-wire_w/2), int(y_size/2-wire_h/2)]
    elements[wire_o[0]:wire_o[0]+wire_w, wire_o[1]:wire_o[1]+wire_h] = [2,0,0,0]

# Now let's iterate through every element and for each one, find the impact of every PM in the array
# Switch to make sure you are only checking magnet cells 
    for i in range(len(elements)):
        for j in range(len(elements[i])):
            pos = np.array([i,j])
            for im in range(len(elements)):
                for jm in range(len(elements[i])):
                    materialSpecial = elements[im][jm][0]
                    if materialSpecial >= 1 and (i,j) != (im,jm):
                        m_pos = np.array([im,jm])
                        r = pos - m_pos
                        # First determine the vector r pointing from the dipole to the current pos
                        internalCheck = 0
                        if i >= mag_o[0] and i <= mag_o[0]+mag_w-1:  # if we are inside the magnet
                            internalCheck += 1
                            if j >= mag_o[1] and j <= mag_o[1]+mag_h-1:
                                internalCheck += 1
                                if im >= mag_o[0] and im <= mag_o[0]+mag_w-1:  # if we are inside the magnet
                                    internalCheck += 1
                                    if jm >= mag_o[1] and jm <= mag_o[1]+mag_h-1:
                                        internalCheck += 1
                        if materialSpecial == 1:
                            temp = calc_dipo_H(r,internalCheck)
                        elif materialSpecial == 2:
                            temp = calc_biot_H(r)
                        #temp = calc_biot_H(r)
                        elements[i,j][1] += temp[1] # artificially swapped order here to make plotting work
                        elements[i,j][2] += temp[0]
# Now let's convert those mx and my vectors to magnitude for easier plotting
    magnitudes = np.zeros((x_size,y_size))
    for i in range(len(elements)):
        for j in range(len(elements[i])):
            mag_vector = np.array([elements[i,j][1],elements[i,j][2]])
            elements[i,j][3] = np.sqrt(mag_vector.dot(mag_vector))
            magnitudes[i][j] = elements[i,j][3]

# PLOTTING
    fig, ax = plt.subplots()
    #plt.imshow(magnitudes)#, interpolation='nearest')
    x,y = np.meshgrid(np.linspace(0,x_size,x_size),np.linspace(0,y_size,y_size))
    u = np.zeros((x_size,y_size))
    v = np.zeros((x_size,y_size))
    m = np.zeros((x_size,y_size))
    for i in range(len(elements)):
        for j in range(len(elements[i])):
            r = np.array([elements[i,j][1],elements[i,j][2]])
            u[i,j] = elements[i,j][1]
            v[i,j] = elements[i,j][2]
            m[i,j] = elements[i,j][3]
            # Normalize here, only applied to the quiver vectors
            u[i,j] = u[i,j]/(m[i,j]**.95) # name the exponent <1 to scale favorably
            v[i,j] = v[i,j]/(m[i,j]**.95)
    m_log = np.log(m)
    qq=plt.quiver(x,y,u,v,m,cmap=plt.cm.jet,norm=colors.LogNorm(vmin=m.min(), vmax=m.max()),width=.0025) # magnitude is still being used for color which is why this works
    plt.colorbar(qq, cmap=plt.cm.jet)

    # Add borders for Magnets and Wires
    mag1 = patches.Rectangle((np.flip(mag_o)), mag_h, mag_w, linewidth=1, edgecolor='r', facecolor='none')
    wire1 = patches.Rectangle((np.flip(wire_o-np.array([.25,.0]))), wire_h, wire_w, linewidth=1, edgecolor='g', facecolor='none')
    ax.add_patch(mag1)
    ax.add_patch(wire1)
    #ax = plt.gca()
    #ax.set_ylim(ax.get_ylim()[::-1])
    #plt.gca().invert_yaxis()
    plt.show()