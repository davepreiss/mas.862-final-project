from pickle import FALSE
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as patches
import matplotlib.colors as colors
from pitFinalMaterials import *
from pitFinalPhysics import *

out_path = "output/magnet_b_field"
iterations = 10

# PHYSICAL PARAMETERS
u0 = 4e-7*np.pi
u_steel = u0*5000 # relative permeability of steel = 5000, permeability = 5000*u0
ur = u_steel / u0
Xm = ur - 1

ferriteON = False

if __name__ == '__main__':
# BUILD 2D SIMULATION
    x_size = 31
    y_size = 31
    elements = np.zeros((x_size,y_size,4))
    ''' elements[0] is a tristate where 0=air, 1=dipole, and 2=wire (no longer used)
        elements[1] is the x component of the H field
        elements[2] is the y component of the H field
        elements[3] is the magnitude of that H vector '''
    magnets, wires, ferritics = materials(x_size, y_size)

    # Now let's iterate through every element and for each one, find the impact of every PM in the array
    for i in range(x_size):
        for j in range(y_size):
            pos = np.array([i,j])
            # Here we want to calculate the effect of an entire magnet and/or an entire wire
            for wire in wires:
                wireElements = wire[4]
                for wireElement in wireElements:
                    r = pos - wireElement # r is the distanc between the current point [i,j] and the wire element
                    if np.linalg.norm(r) != 0: # don't try to caclulate for r = 0 (there's no curl contributed)
                        elements[i,j][1] += calc_biot_B(wire[0], r)[0]
                        elements[i,j][2] += calc_biot_B(wire[0], r)[1]
            for magnet in magnets:
                ox, oy = magnet[3] # center of the magnet
                x_pos, y_pos = pos - [ox,oy] # current x and y positions
                mx, my = magnet[0] # magnetization vector
                hx, hy = magnet[1], magnet[2] # height of the magnet
                elements[i,j][1] += calc_magnet_B(x_pos, y_pos, mx, my, hx, hy, ox, oy)[0]
                elements[i,j][2] += calc_magnet_B(x_pos, y_pos, mx, my, hx, hy, ox, oy)[1]
    
    # Now lets store all of our initially induced B fields permanenetly 
    B_induced = elements

    if len(ferritics) == 0:
        iterations = 1
    for iteration in range(iterations):
        if ferriteON == True:
            # Now let's assign our ferritic regions and update their B fields with a new u
            for ferrite in ferritics:
                i = ferrite[3][0]
                j = ferrite[3][1]
                ferrite[0] = np.array([elements[i,j][1], elements[i,j][2]]) * ur # set the m fields here which is B * ur

            for i in range(x_size):
                for j in range(y_size):
                    pos = np.array([i,j])
                    for ferrite in ferritics:
                        ox, oy = ferrite[3] # center of the magnet
                        x_pos, y_pos = pos - [ox,oy] # current x and y positions
                        mx, my = ferrite[0] # magnetization vector
                        hx, hy = 1, 1 # height of the magnet
                        elements[i,j][1] = B_induced[i,j][1] + calc_magnet_B(x_pos, y_pos, mx, my, hx, hy, ox, oy)[0]
                        elements[i,j][2] = B_induced[i,j][2] + calc_magnet_B(x_pos, y_pos, mx, my, hx, hy, ox, oy)[1]
        ferriteON = True
        # Now let's put every x and y value into its own array for easier plotting with quiver
        x_samples = []
        y_samples = []
        x_vals = []
        y_vals = []
        storage = []
        for i in range(x_size):
            for j in range(y_size):
                x_samples.append(i)
                y_samples.append(j)
                x_vals.append(elements[i,j][1])
                y_vals.append(elements[i,j][2])
                if i == round(x_size/2) and j >= 5:
                    print(elements[i,j][2])
                    storage.append(elements[i,j][2])
        magnitudes = np.sqrt(np.square(x_vals) + np.square(y_vals))
        # Plotting
        fig, ax = plt.subplots()
        ax.set_aspect(1.0)

        norm = colors.LogNorm()
        norm.autoscale(magnitudes)
        cm = plt.cm.jet
        sm = plt.cm.ScalarMappable(cmap=cm, norm=norm)
        sm.set_array([])
        abc = ax.quiver(x_samples, y_samples, x_vals/magnitudes, y_vals/magnitudes, color=cm(norm(magnitudes)))
        fig.colorbar(sm)

        # Add borders for Magnets and Wires, and Ferrites-?
        for wire in wires:
            wirePatch = patches.Rectangle(wire[3] - np.array([0.5,0.5]), wire[1], wire[2], linewidth=1, edgecolor='g', facecolor='none')
            ax.add_patch(wirePatch)
        for magnet in magnets:
            magnetPatch = patches.Rectangle(magnet[3] - np.array([magnet[1]/2, magnet[2]/2]), magnet[1], magnet[2], linewidth=1, edgecolor='r', facecolor='none')
            ax.add_patch(magnetPatch)
        for ferrite in ferritics:
            ferritePatch = patches.Rectangle(ferrite[3] - np.array([ferrite[1]/2, ferrite[2]/2]), ferrite[1], ferrite[2], linewidth=.5, edgecolor='b', facecolor='none')
            ax.add_patch(ferritePatch)

        # Save the figure.
        fig.savefig(out_path + str(iteration) + ".png", dpi=150)
        plt.close(fig)
        #plt.show()