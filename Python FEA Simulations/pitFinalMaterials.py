import numpy as np

def materials(x_size, y_size):
    """Assign our materials of interests
    """
    # CREATE MAGNETS
    magnets = [] # Now let's assign regions of permanent magnetic material
    def magnetMaker(a, b, c, d):
        m = np.array(a) # Bohr Magneton is 9.274e-24 J/T for context
        mag_w = b # x dimension - must be odd
        mag_h = c # y dimension - must be odd
        mag_o = d # origin
        magnets.append([m, mag_w, mag_h, mag_o,])
    # syntax is: ferriteMaker([mx,my], mag_w, mag_h, [mag_ox, mag_oy])
    offset = -10
    magnetMaker([0,1],3,9,[round(x_size/2)+offset, round(y_size/2)-1])

    # CREATE WIRES
    wires = [] # Now let's assign a region of current carrying wire
    def wireMaker(a, b, c, d):
        Idl = np.array((0,0,a)) # current flow vector for biot-savart
        wire_w = b
        wire_h = c
        wire_o = [d[0],d[1]]#[int(x_size/2-wire_w/2), int(y_size/2-wire_h/2)]
        coords = []
        for i in range(wire_w):
            for j in range(wire_h):
                wirePos = [wire_o[0] + i, wire_o[1] + j]
                coords.append(wirePos)
        wires.append([Idl, wire_w, wire_h, wire_o, coords])
    # syntax is: wireMaker(Idl,wire_w,wire_h,[x_center, y_center])
    z = 1
    #wireMaker(z,2,1,[x_size/2 - 24, 5])
    #wireMaker(z,2,1,[x_size/2 - 20, 5])
    #wireMaker(z,2,1,[x_size/2 - 16, 5])
    wireMaker(z,2,1,[x_size/2 - 12, 5])
    wireMaker(z,2,1,[x_size/2 - 8, 5])
    wireMaker(z,2,1,[x_size/2 - 4, 5])

    #wireMaker(-z,2,1,[x_size/2 + 24, 5])
    #wireMaker(-z,2,1,[x_size/2 + 20, 5])
    #wireMaker(-z,2,1,[x_size/2 + 16, 5])
    wireMaker(-z,2,1,[x_size/2 + 12, 5])
    wireMaker(-z,2,1,[x_size/2 + 8, 5])
    wireMaker(-z,2,1,[x_size/2 + 4, 5])
    for wire in wires:
        wire[3][0] -= 1
    
    # CREATE FERITIC MATERIALS
    ferritics = [] # Now let's assign a region of current carrying wire
    def ferriteMaker(a, b, c, d):
        M = np.array([0,0]) # current flow vector for biot-savart
        ferr_w = b
        ferr_h = c
        ferr_o = [d[0],d[1]]#[int(x_size/2-wire_w/2), int(y_size/2-wire_h/2)]
        for i in range(ferr_w):
            for j in range(ferr_h):
                ferrPos = [ferr_o[0] + i, ferr_o[1] + j]
                ferritics.append([M, 1, 1, ferrPos])
    # syntax is: ferriteMaker(m,1,1,[x_pos, y_pos])
    #ferriteMaker(0,4,15,[round(x_size/2) + 0, round(y_size/2)-1 + 2 - 9])
    
    

    # Now return everything
    return magnets, wires, ferritics